package solution;

import java.util.Comparator;
import java.util.List;

public class Comparators {
    public static class ScalarComparator implements Comparator<Measurement> {

        @Override
        public int compare(Measurement o1, Measurement o2) {
            return Double.compare(o1.getScalarTime(), o2.getScalarTime());
        }
    }

    public static class VectorComparator implements Comparator<Measurement> {

        @Override
        public int compare(Measurement o1, Measurement o2) {
            int sum = 0;
            List<Integer> vectorList1 = o1.getVectorOfClients();
            for(int i = 0; i < vectorList1.size(); ++i){
                sum += Double.compare(vectorList1.get(i), o2.getVectorOfClients().get(i));
            }
            return sum;
        }
    }
}
