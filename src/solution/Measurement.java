package solution;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Measurement implements Serializable {

    private double CO;
    private double scalarTime = 0;
    private boolean confirmed = false;
    private int senderPort = 0;
    private int recieverPort = 0;
    private List<Integer> vectorOfClients = new ArrayList<>();

    public Measurement(double CO, double scalarTime, boolean confirmed, int senderPort, int recieverPort, List<Integer> vectorOfClients) {
        this.CO = CO;
        this.scalarTime = scalarTime;
        this.confirmed = confirmed;
        this.senderPort = senderPort;
        this.recieverPort = recieverPort;
        this.vectorOfClients = vectorOfClients;
    }

    public double getCO() {
        return CO;
    }

    public double getScalarTime() {
        return scalarTime;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void confirm(boolean confirmed) {
        this.confirmed = confirmed;
    }

    public int getSenderPort() {
        return senderPort;
    }

    public int getRecieverPort() {
        return recieverPort;
    }

    public List<Integer> getVectorOfClients() {
        return vectorOfClients;
    }

    @Override
    public String toString() {
        return "Measurement{" +
                "CO=" + CO +
                ", scalarTime=" + scalarTime +
                ", confirmed=" + confirmed +
                ", senderPort=" + senderPort +
                ", recieverPort=" + recieverPort +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Measurement)) return false;
        Measurement that = (Measurement) o;
        return Double.compare(that.getCO(), getCO()) == 0 &&
                Double.compare(that.getScalarTime(), getScalarTime()) == 0 &&
                getSenderPort() == that.getSenderPort() &&
                getRecieverPort() == that.getRecieverPort();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCO(), getScalarTime(), getSenderPort(), getRecieverPort());
    }
}
