package solution;

import hr.fer.tel.rassus.stupidudp.network.EmulatedSystemClock;
import hr.fer.tel.rassus.stupidudp.network.SimpleSimulatedDatagramSocket;

import java.io.*;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Node3 {

    public static void main(String[] args) throws IOException, InterruptedException {

        long startTime = System.nanoTime();
        EmulatedSystemClock emulatedSystemClock = new EmulatedSystemClock();
        List<Measurement> measurementList = new ArrayList<>();
        List<Integer> otherClients = new ArrayList<>();
        otherClients.add(6787);
        otherClients.add(6687);
        List<Integer> vectorOfClients = new ArrayList<>();
        vectorOfClients.add(0);//client 6587
        vectorOfClients.add(0);//client 6687
        vectorOfClients.add(0);//client 6787

        SimpleSimulatedDatagramSocket serverSocket = new SimpleSimulatedDatagramSocket(6587, 0, 1000);
        new Thread(() -> {
            //Primanje mjerenja------------------------------------------------------------------------------------------------
            while (true) {
                byte[] recBytes = new byte[1000];
                DatagramPacket packet = new DatagramPacket(recBytes, 1000);
                try {
                    serverSocket.receive(packet);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                ObjectInputStream iStream = null;
                try {
                    iStream = new ObjectInputStream(new ByteArrayInputStream(recBytes));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Measurement messageClass = null;
                try {
                    messageClass = (Measurement) iStream.readObject();
                    iStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                System.out.println("Recieved: " + messageClass.toString());
                if (!measurementList.contains(messageClass)) {
                    measurementList.add(messageClass);
                    if (messageClass.getSenderPort() == 6588)
                        vectorOfClients.set(0, vectorOfClients.get(0) + 1);
                    if (messageClass.getSenderPort() == 6688)
                        vectorOfClients.set(1, vectorOfClients.get(1) + 1);
                    if (messageClass.getSenderPort() == 6788)
                        vectorOfClients.set(2, vectorOfClients.get(2) + 1);

                    //Provjera sata
                    if (emulatedSystemClock.currentTimeMillis() < messageClass.getScalarTime()) {
                        System.out.println("Change of time initiated...old time: " + emulatedSystemClock.currentTimeMillis() + ", new time: " + messageClass.getScalarTime());
                        emulatedSystemClock.setStartTime((long) messageClass.getScalarTime());
                    }
                }

                //Slanje potvrde posaljitelju--------------------------------------------------------------
                messageClass.confirm(true);

                try {
                    ByteArrayOutputStream bStream = new ByteArrayOutputStream();
                    ObjectOutput oo = new ObjectOutputStream(bStream);
                    oo.writeObject(messageClass);
                    oo.close();

                    byte[] serializedMessage = bStream.toByteArray();
                    serverSocket.send(new DatagramPacket(serializedMessage, serializedMessage.length, InetAddress.getLocalHost(), messageClass.getSenderPort()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        SimpleSimulatedDatagramSocket clientSocket = new SimpleSimulatedDatagramSocket(6588, 0, 1000);
        ByteArrayOutputStream bStream = new ByteArrayOutputStream();
        ObjectOutput oo = new ObjectOutputStream(bStream);
        oo.writeObject(new Measurement(1, emulatedSystemClock.currentTimeMillis(), false, 6588, 6687, vectorOfClients));
        oo.close();

        byte[] serializedMessage = bStream.toByteArray();
        //clientSocket.send(new DatagramPacket(serializedMessage, serializedMessage.length, InetAddress.getLocalHost(), 6687));

        new Thread(() -> {
            try {
                Thread.sleep(50000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Collections.sort(measurementList, new Comparators.ScalarComparator());
            System.out.println("Scalar time order: " + measurementList.toString());
            double sum = 0;
            for (Measurement measurement : measurementList)
                sum += measurement.getCO();
            System.out.println("Average CO: " + (sum / measurementList.size()));
            System.out.println("Vector: " + vectorOfClients.toString());
            Collections.sort(measurementList, new Comparators.VectorComparator());
            System.out.println("Vector time order: " + measurementList.toString());
            measurementList.clear();
        }).start();

        //Handling send part...

        while (true) {
            Thread.sleep(10000);

            long endTime = System.nanoTime();
            double difference = ((endTime - startTime) / (Math.pow(10, 10)));
            difference = Math.round(difference);
            //System.out.println("here");
            //System.out.println("Active seconds " + difference);
            int lineToRead = (int) difference % 100 + 2;
            //System.out.println("Preparing to read line number " + lineToRead);
            String measurementLine = "";
            try {
                Scanner scanner = new Scanner(new FileInputStream("C:\\Users\\lukak\\Documents\\rassus-homework-1-client\\client\\src\\mjerenja.csv"));
                int i = 1;
                while (scanner.hasNextLine()) {
                    if (i == lineToRead) {
                        measurementLine = scanner.nextLine();
                        break;
                    }
                    ++i;
                    scanner.nextLine();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            //System.out.println("Line: " + measurementLine);
            String[] lineArray = measurementLine.split(",");

            int CO = Integer.parseInt(lineArray[3]);
            measurementList.add(new Measurement(CO, emulatedSystemClock.currentTimeMillis(), true, 0, 0, vectorOfClients));

            //Sending data----------------------------------------------------------------------------------------------------
            for (int port : otherClients) {
                bStream = new ByteArrayOutputStream();
                oo = new ObjectOutputStream(bStream);
                oo.writeObject(new Measurement(CO, emulatedSystemClock.currentTimeMillis(), false, 6588, port, vectorOfClients));
                oo.close();

                serializedMessage = bStream.toByteArray();
                clientSocket.send(new DatagramPacket(serializedMessage, serializedMessage.length, InetAddress.getLocalHost(), port));
                System.out.println("Sending: " + new Measurement(CO, emulatedSystemClock.currentTimeMillis(), false, 6588, port, vectorOfClients).toString());

                vectorOfClients.set(0, vectorOfClients.get(0) + 1);
                //Waiting for confirmation-----------------------------------------------------------------------------------------
                while (true) {
                    clientSocket.setSoTimeout(5000);
                    byte[] recBytes = new byte[1000];
                    DatagramPacket packet = new DatagramPacket(recBytes, 1000);
                    try {
                        clientSocket.receive(packet);
                    } catch (IOException e) {
                        e.printStackTrace();
                        continue;
                    }
                    ObjectInputStream iStream = null;
                    try {
                        iStream = new ObjectInputStream(new ByteArrayInputStream(recBytes));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Measurement messageClass = null;
                    try {
                        messageClass = (Measurement) iStream.readObject();
                        if (messageClass == null)
                            continue;
                        iStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Recieved confirmed: " + messageClass.toString());
                    break;
                }
            }
        }
    }
}

